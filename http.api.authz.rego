package http.api.authz

import input
import future.keywords.in
import future.keywords.contains
import future.keywords.if

default allow := false

# TODO: inject it via the env vars
base_user_permissions_url := "http://127.0.0.1:8484/users_permissions/"

# Fetch the user email from the headers
user_email := input.request.headers["http-x-simba-sub"]
user_id := input.request.headers["http-x-simba-sub-id"]

# Generic rule that fits most of the endpoints
allow {
    user_permissions_url := concat("", [base_user_permissions_url, "?user_email=", user_email, "&service=", input.request.service, "&resource=", input.request.resource, "&action=", input.request.action])

    response := http.send({"method": "get", "url": user_permissions_url})
    user_permissions := response.body
    user := user_permissions.user
    user_orgs := [org | org := user.organisations[_].name]

    some perm in user_permissions.permissions
    perm.effect == "allow"
    perm.service == input.request.service
    perm.resource == input.request.resource
    perm.action == input.request.action
    not any_resource_attribute_not_allowed(perm.resource_attributes, user_orgs)
}

any_resource_attribute_not_allowed(resource_attributes, user_orgs) {
    some key
    value := resource_attributes[key]
    not is_resource_attribute_allowed(key, value, user_orgs)
}

is_resource_attribute_allowed(key, value, user_orgs) {
    key == "organisation_name"
    value == "same-as-user"
    input.request.attributes[key] in user_orgs
}

is_resource_attribute_allowed(key, value, user_orgs) {
    key == "domain_name"
    value == "same-as-user"
    input.request.attributes[key] in user_orgs
}

is_resource_attribute_allowed(key, value, user_orgs) {
    input.request.attributes[key] == value
}
### END OF GENERIC RULE THAT FITS MOST OF THE ENDPOINTS

# Allow user to read it's own account
allow {
    input.request.service == "member"
    input.request.resource == "user_accounts"
    input.request.action == "read"
    input.request.attributes["user_account_id"] == user_id
}

# Allow user to read it's own profile
allow {
    input.request.service == "member"
    input.request.resource == "user_profiles"
    input.request.action == "read"
    input.request.attributes["user_account_id"] == user_id
}

# Allow user to update it's own profile
allow {
    input.request.service == "member"
    input.request.resource == "user_profiles"
    input.request.action == "update"
    input.request.attributes["user_account_id"] == user_id
}