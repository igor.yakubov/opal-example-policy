package contact.contracts.httpapi.authz.read

import input
import future.keywords.in
import future.keywords.contains
import future.keywords.if

import data.users_db.users

default allow := false


allow {
    response := http.send({"method": "get", "url": "http://localhost:8585/get-hello/"})
    print(response)
    count(filtered_permissions) > 0
#    count(filtered_pemissions_by_org_attribute) > 0
}

# Filter permission by Action (list, read, create, ...)
# In the actual implementation the filters will inlude the service and resource names
# It will be done in the Member Service, when fetching the permissions
filtered_permissions contains perm if {
    some perm in users["alice"].permissions
    perm.action == "read"
}

############ Org Attribute Rules #########################
# filter permissions by org_id == "same-as-user"
filtered_pemissions_by_org_attribute contains perm if {
    some perm in filtered_permissions
    perm.resource_attributes.org_id == "same-as-user"
    input.request.attributes.org_id in users["alice"].orgs
}

# filter permissions by org_id == "*"
filtered_pemissions_by_org_attribute contains perm if {
    some perm in filtered_permissions
    perm.resource_attributes.org_id == "*"
}

# filter permissions by org_id == "some-specific-org"
filtered_pemissions_by_org_attribute contains perm if {
    some perm in filtered_permissions
    input.request.attributes.org_id == perm.resource_attributes.org_id
}
############# End of Org Attribute Rules ##################

# TODO: fetch contract info from the service
############ App-Name Attribute Rules #####################
#filtered_permissions_by_app_name_attribute contains perm if {
#    some perm in filtered_pemissions_by_org_attribute
#    perm.attributes.app_name == "*"
#}
#
#filtered_permissions_by_app_name_attribute contains perm if {
#    some perm in filtered_pemissions_by_org_attribute
#    perm.attributes.app_name == input.request.attributes.app_name
#}
############ End of App-Name Attribute Rules ##############
#
############ Internal BC ID Attribute Rules #####################
#filtered_permissions_by_internal_bc_id_attribute contains perm if {
#    some perm in filtered_permissions_by_app_name_attribute
#    perm.attributes.internal_bc_id == "*"
#}
#
#filtered_permissions_by_internal_bc_id_attribute contains perm if {
#    some perm in filtered_pemissions_by_org_attribute
#    perm.attributes.internal_bc_id == input.request.attributes.internal_bc_id
#}
############ End of Internal BC ID Attribute Rules ##############

########### Contract ID Attribute Rules #####################
filtered_permissions_by_contract_id_attribute contains perm if {
    some perm in filtered_pemissions_by_org_attribute
    perm.resource_attributes.contract_id == "*"
}

filtered_permissions_by_contract_id_attribute contains perm if {
    some perm in filtered_pemissions_by_org_attribute
    perm.resource_attributes.contract_id == input.request.attributes.contract_id
}
########### End of Internal BC ID Attribute Rules ##############




FROM LIST POLICY FIRST ITTERATION
############ Org Attribute Rules #########################
## filter permissions by org_id == "same-as-user"
#filtered_pemissions_by_org_attribute contains perm if {
#    some perm in filtered_permissions
#    perm.resource_attributes.org_id == "same-as-user"
#    input.request.attributes.org_name in users["alice"].orgs
#}
#
## filter permissions by org_id == "*"
#filtered_pemissions_by_org_attribute contains perm if {
#    some perm in filtered_permissions
#    perm.resource_attributes.org_id == "*"
#}
#
## filter permissions by org_id == "some-specific-org"
#filtered_pemissions_by_org_attribute contains perm if {
#    some perm in filtered_permissions
#    input.request.attributes.org_name == perm.resource_attributes.org_id
#}
############# End of Org Attribute Rules ##################


