package users_db

users := {
    "alice": {
        "orgs": ["org-A-id", "org-B-id"],
        "permissions": [
             {
                "effect": "allow",
                "name": "Default Read Contracts.",
                "description": "Allows to read all contracts from the Org that user belongs to.",
                "service": "contract",
                "resource": "contracts",
                "action": "delete",

                "resource_attributes": {
                    "org_name": "same-as-user",
                    "app_name": "*",
                    "internal_bc_id": "id-1",
                    "contract_id": "*",
                },
                "request_conditions": {
                    "request_from_ip": "127.0.0.1",
                }
            },
            {
                "effect": "allow",
                "name": "Read Contracts-A from the same org as user.",
                "description": "Allows to read all contracts from the Org that user belongs to.",
                "service": "contract",
                "resource": "contracts",
                "action": "read",
                "resource_attributes": {
                    "org_name": "*",
                    "app_name": "*",
                    "internal_bc_id": "*",
                    "contract_id": "*",
                },
                "request_conditions": {
                    "request_from_ip": "127.0.0.1",
                }
            },
            {
                "effect": "allow",
                "name": "List Contracts",
                "description": "Allows to list all contracts from the Org that user belongs to.",
                "service": "contract",
                "resource": "contracts",
                "action": "list",
                "resource_attributes": {
                    "org_name": "same-as-user",
                    "app_name": "*",
                },
                "request_conditions": {
                    "request_from_ip": "127.0.0.1",
                }
            },
            {
                "effect": "allow",
                "name": "List Org-A Contracts",
                "description": "Allows to list all contracts from a specific Org.",
                "service": "contract",
                "resource": "contracts",
                "action": "list",
                "resource_attributes": {
                    "org_name": "org-C-id",
                    "app_name": "*",
                    "internal_bc_id": "*",
                },
                "request_conditions": {
                    "request_from_ip": "127.0.0.1",
                }
            },
#            {
#                "name": "List All Contracts",
#                "description": "Allows to list contracts from all orgs",
#                "service": "contract",
#                "resource": "contracts",
#                "action": "list",
#                "resource_attributes": {
#                    "org_name": "*",
#                    "app_name": "*",
#                    "internal_bc_id": "*",
#                }
#            },
            {
                "effect": "allow",
                "name": "List App-A Contracts",
                "description": "Allows to list contracts under APP-A",
                "service": "contract",
                "resource": "contracts",
                "action": "list",
                "resource_attributes": {
                    "org_name": "same-as-user",
                    # TODO: how do we handle "app_name": "*" above
                    "app_name": "app-A-name",
                    "internal_bc_id": "*",
                },
                "request_conditions": {
                    "request_from_ip": "127.0.0.1",
                }
            },
        ]
    }
}