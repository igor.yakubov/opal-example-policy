package httpapi.authz
#package system.main

#import input

default allow := false

allow {
    print(input)
}

## Helper to get the token payload.
#token := {"payload": payload} {
#	[header, payload, signature] := io.jwt.decode(input.headers["x-access-token"])
#}
#
#trimmed_path := trim(input.path, "/")
#splitted_path := split(trimmed_path, "/")
#
#allow {
#    print("inside allow...")
#    # user belongs to an organization
#    token.payload.org == splitted_path[0]
#    # user has the admin role
#    token.payload.realm_access.roles[_] == "org-admin"
#}
